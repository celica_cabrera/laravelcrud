@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-xs-12">
            <a href="{{ route('products.index') }}" class="btn color-blue pull-right"><i class="fa fa-cubes"></i>Products</a>
        </div>
        <div class="row">
            <form action="{{ route('products.update', $product->id) }}" method="POST">
                {{ csrf_field() }}
                @include('products.components.form')
                <input  type="hidden" name="_method" value="PUT" />
                <input type='submit' class='btn btn-primary' value='ENVIAR' />
            </form>
        </div>
    </div>
@endsection