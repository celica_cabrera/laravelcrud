@extends('layouts.app')

@section('content')
   <div class="container">

      <div class="col-xs-12">
          <a href="{{ route('products.create') }}" class="btn color-blue pull-right"><i class="fa fa-cube"></i> <i class="fa fa-plus"></i></a>

          @if(Session::has('info'))
              <input type="hidden" class="info" value="{{ Session::get('info') }}">
          @endif
      </div>
      <div class="row">
          <table class="table table-striped">
              <thead>
              <th>Name</th>
              <th>Description</th>
              <th>Body</th>
              <th>Edit</th>
              <th>Delete</th>
              </thead>
              <tbody>
              @foreach($products as $product)
                  <tr>
                      <td>{{ $product->name }}</td>
                      <td>{{ $product->description }}</td>
                      <td>{{ $product->body }}</td>
                      <td>
                          <form action="{{ route('products.edit', $product->id) }}" method="GET">
                              {{ csrf_field() }}
                              <input  type="hidden" name="_method" value="GET" />
                              <button class="btn btn-default color-blue"><i class="fa fa-pencil"></i></button>
                          </form></td>
                      <td>
                          <form class="form-delete" action="{{ route('products.destroy', $product->id) }}" method="POST">
                              {{ csrf_field() }}
                              <input  type="hidden" name="_method" value="DELETE" />
                              <a class="btn btn-default btn-delete color-red"><i class="fa fa-trash"></i></a>
                          </form>
                      </td>
                  </tr>
              @endforeach
              </tbody>
          </table>
      </div>

   </div>

@endsection

@section('script')
    <!-- Javascript local page -->
    <script src="{!! asset('js/products/delete.js') !!}"></script>
    <script src="{!! asset('js/products/index.js') !!}"></script>
@endsection