
 <div class="col-xs-12">
     <div class="form-group">
         <label for="name">Product name</label>
         <input type="text" id="name" name="name" class="form-control" value="@if(!empty($product->name)) {{ $product->name }} @endif" />
     </div>

     <div class="form-group">
         <label for="description">Description</label>
         <input type="text" id="description" name="description" class="form-control" value="@if(!empty($product->description)) {{ $product->description }} @endif" />
     </div>

     <div class="form-group">
         <label for="body">Body</label>
         <input type="text" id="body" name="body" class="form-control" value="@if(!empty($product->body)) {{ $product->body }}  @endif" />
     </div>
 </div>
