/**
 * Functions to delete a product
 */
var sendFormButtonsDelete = {};
var formsDelete = {};

function initVars() {
    sendFormButtonsDelete =  $('.btn-delete');
    formsDelete =  $('.form-delete');
}

$(document).ready(function(){
    initVars();
    sendFormButtonsDelete.each(function(index){

        $(this).on('click', function(){

            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this product!",
                icon: "warning",
                buttons: true,
                dangerMode: true,

            })
                .then((willDelete) => {
                    if (willDelete) {
                        submitForm(formsDelete.get(index));
                    } 
                });

        });
    });

});

function  submitForm(form) {
    $(form).submit();
}