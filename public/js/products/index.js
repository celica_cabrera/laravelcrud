var infoAlert = {};
var message = "";

function initInfo() {
    infoAlert =  $('.info');

}

$(document).ready(function(){
    initInfo();
    message = infoAlert.val();
    if(message.length > 0){
        swal({
            title: message,
            icon: "success",
            timer: 2800,
            button: {
                visible: false
            }
        });
        message = "";
    }

});